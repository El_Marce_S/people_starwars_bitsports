import 'package:flutter/material.dart';
import 'package:starwars_people/model/person_model.dart';
import 'package:starwars_people/services/swapi_service.dart';

class SwPersonListView extends StatelessWidget {
  final List<SwApiService> persons;

  const SwPersonListView({
    Key? key,
    required this.persons,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
        itemCount: persons.length,
        itemBuilder: (context, index) {
          return Text('data $index');

        },
      ),
    );
  }
}

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:starwars_people/bloc/home_bloc.dart';
import 'package:starwars_people/model/person_model.dart';

import 'package:starwars_people/services/swapi_service.dart';

import 'package:starwars_people/ui/shared/shared_appbar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomeBloc(
        RepositoryProvider.of<SwApiService>(context),
      )..add(LoadApiEvent()),
      child: Scaffold(
        appBar: const SharedAppBar(),
        body: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            if (state is HomeInitial) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CupertinoActivityIndicator(),
                  Text('Loading data'),
                ],
              );
            }
            if (state is HomeLoadedState) {
              return ListView.builder(
                itemCount: state.persons.length,
                itemBuilder: (BuildContext context, int index) {
                  final SwPerson singlePerson = state.persons[index];
                  return ListTile(
                    title: Text(singlePerson.name),
                    subtitle: Text(singlePerson.species.toString() + 'from ' + singlePerson.homeworld),
                    trailing: const Icon(Icons.arrow_forward_ios),
                    onTap: () {
                      print('Mandaremos a otra pagina');
                    },
                  );
                },
              );
            }
            return Container();
          },
        ),
      ),
    );
  }
}

// To parse this JSON data, do
//
//     final people = peopleFromMap(jsonString);

import 'dart:convert';

import 'package:starwars_people/model/person_model.dart';



class SwApiResponse {
  SwApiResponse({
    required this.count,
    this.next,
    this.previous,
    required this.results,
  });

  int count;
  String? next;
  dynamic previous;
  List<SwPerson> results;

  factory SwApiResponse.fromJson(String str) => SwApiResponse.fromMap(json.decode(str));

  factory SwApiResponse.fromMap(Map<String, dynamic> json) => SwApiResponse(
        count: json["count"],
        next: json["next"],
        previous: json["previous"],
        results: List<SwPerson>.from(json["results"].map((x) => SwPerson.fromMap(x))),
      );
}

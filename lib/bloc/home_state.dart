part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class HomeLoadedState extends HomeState {
  // final String personName;
  // final int species;
  // final String homeworld;

  final List persons;

  HomeLoadedState(this.persons);

  @override
  List<Object> get props => [persons];
}

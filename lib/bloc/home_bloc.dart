import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:starwars_people/services/swapi_service.dart';

part 'home_event.dart';

part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final SwApiService _swApiService;

  HomeBloc(this._swApiService) : super(HomeInitial()) {
    on<LoadApiEvent>((event, emit) async {
      final personsList = await _swApiService.getAllPersons();
      emit(HomeLoadedState(personsList));
    });
  }
}

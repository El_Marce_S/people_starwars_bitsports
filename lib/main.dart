import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:starwars_people/services/swapi_service.dart';
import 'package:starwars_people/ui/pages/home/home_page.dart';
import 'package:starwars_people/ui/shared/shared_appbar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // routes: {
      //   '/home': (context) => const HomePage(),
      // },
      // initialRoute: '/home',
      title: 'StarWars Persons',
      home: MultiRepositoryProvider(
        providers: [
          RepositoryProvider(
            create: (context) => SwApiService(),
          )
        ], child: HomePage(),
      ),
    );
  }
}

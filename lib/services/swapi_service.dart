
import 'package:http/http.dart' as http;

import '../model/person_model.dart';
import '../model/swapi_persons_response_model.dart';

List<SwPerson> allPersonsList = [];

class SwApiService {
  getAllPersons() async {
    var url = Uri.parse('https://swapi.dev/api/people');
    final response = await http.get(url);
    final allPersons = SwApiResponse.fromJson(response.body);
    allPersonsList = allPersons.results;
    return allPersonsList;
  }
}
